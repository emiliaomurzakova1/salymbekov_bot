import json

def save_user(chat_id, first_name, last_name, language):
    with open(file="db/user.json", mode="r") as file:
        users = file.read()
        users_dict = json.loads(users)
    if users_dict.get(chat_id) is not None:
        return False
    users_dict[chat_id] = {        
        "first_name": first_name,
        "last_name": last_name,
        "language": language
    }
    with open(file="db/user.json", mode="w") as file:
        file.write(json.dumps(users_dict))
    return True
    #  TODO фиксануть баг с добавлением пользователей 